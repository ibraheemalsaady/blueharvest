require('dotenv').config();

const path = require('path');

try {
  require(path.join('..', '/dist/bundle.js'));
} catch (err) {
  console.error(err);
}