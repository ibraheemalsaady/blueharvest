
const fs = require('fs');

const babelRc = JSON.parse(fs.readFileSync('config/.babelrc.dev', 'utf-8'));

const obj = Object.assign(
  {},
  babelRc,
  { ignore: /\/(dist|node_modules)\// },
);

require('babel-register')(obj);
