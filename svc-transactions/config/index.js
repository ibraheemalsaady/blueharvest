/* eslint no-self-compare: 0 */

const config = {
  production: process.env.NODE_ENV === 'production',
  testing: process.env.NODE_ENV === 'test',
  mongoUrl: process.env.MONGO_URL || 'mongodb://localhost:1001/blueharvest',
  rabbitmqUrl: process.env.CLOUDAMQP_URL || 'amqp://localhost',
};

export default config;
