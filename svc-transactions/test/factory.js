/*
    Rosie is a factory for building JavaScript objects, it will basically mock our models.
    Rosie docs: https://github.com/rosiejs/rosie

    Faker is used for creating random/fake data
    Faker docs: https://github.com/marak/Faker.js/
*/
// import { Factory } from 'rosie';
// import Faker from 'faker';

const models = {};

export default models;
