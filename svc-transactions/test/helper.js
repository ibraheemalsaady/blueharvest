import Faker from 'faker';
// import Factory from 'factory';
import { Transaction } from 'models';

/**
 * Creates a mock customer
 * @param {*} overrides overrides the default values of the customer model if provided
 */
async function createTransaction(customerId, accountId, credit = Faker.random.number(100, 9999)) {
  const data = {
    customerId,
    accountId,
    credit,
  };
  const transaction = new Transaction(data);

  await transaction.save();

  return transaction;
}

export default { createTransaction };
