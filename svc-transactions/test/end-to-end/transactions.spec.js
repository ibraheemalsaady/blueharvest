import Helper from 'helper';
import Request from 'supertest';
import app from 'app';

describe('GET: /transactions/:id', () => {
  let customerId = null;

  before(async () => {
    customerId = '123';
    await Helper.createTransaction(customerId, '456');
  });

  it('should get the transactions based on the customer id', async () => {
    const res = await Request(app)
      .get(`/transactions/${customerId}`)
      .expect(200);
  });
});
