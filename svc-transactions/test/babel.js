require('babel-core/register')({
  presets: ['env'],
  plugins: [
    'transform-object-rest-spread',
    ['transform-runtime', {
      polyfill: false,
      regenerator: true,
    }],
    ['module-resolver', {
      root: ['../'],
      alias: {
        app: './src/index',
        config: './config',
        controllers: './src/server/controllers',
        routes: './src/server/routes',
        models: './src/models',
        utils: './src/utils',
        factory: './test/factory',
        helper: './test/helper',
      },
    }],
  ],
});
