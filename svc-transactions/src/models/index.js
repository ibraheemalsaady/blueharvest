import mongoose, { Schema } from 'mongoose';

const { ObjectId } = Schema;

const TransactionSchema = new Schema({
  id: ObjectId,
  customerId: { type: String },
  accountId: { type: String },
  credit: { type: Number },
});

const Transaction = mongoose.model('transactions', TransactionSchema);

export default {
  Transaction,
};

export { Transaction };
