import amqp from 'amqplib';
import config from 'config';
import transactions from 'utils/queues/transactions';

let amqpConn = null;

function closeOnErr(err) {
  if (!err) { return false; }

  amqpConn.close();
  return true;
}

async function startWorker() {
  if (config.testing) { return; }

  try {
    const channel = await amqpConn.createChannel();

    await transactions(channel);
    // await channel.assertQueue('transactions');

    // channel.consume('transactions', (msg) => {
    //   if (msg !== null) {
    //     console.log(msg.content.toString());

    //     channel.ack(msg);
    //   }
    // });
  } catch (err) {
    if (closeOnErr(err)) { return; }

    throw new Error(err);
  }
}

async function whenConnected() {
  await startWorker();
}

async function start() {
  if (config.testing) { return; }

  try {
    amqpConn = await amqp.connect(`${config.rabbitmqUrl}?heartbeat=60`);

    amqpConn.on('error', (err) => {
      console.log('rabbit mq error here...');
      throw new Error(err);
    });
    amqpConn.on('close', () => {
      console.error('[AMQP] reconnecting');
      return setTimeout(start, 1000);
    });

    console.log('rabbitmq connected...');

    whenConnected();

    return amqpConn;
  } catch (err) {
    throw new Error(err);
  }
}

export default {
  start,
};
