import mongoose from 'mongoose';
import config from 'config';

function connect() {
  mongoose.connect(config.mongoUrl);
}

export default connect;
