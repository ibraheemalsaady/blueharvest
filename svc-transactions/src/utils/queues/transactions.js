import { Transaction } from 'models';

const QUEUE_NAME = 'transactions';

async function transactionsQueue(channel) {
  console.log('watching transactions queue...');

  await channel.assertQueue(QUEUE_NAME);

  channel.consume(QUEUE_NAME, (msg) => {
    if (msg !== null) {
      const content = msg.content.toString();
      const obj = JSON.parse(content);

      const transaction = new Transaction({
        customerId: obj.customerId,
        accountId: obj.accountId,
        credit: obj.initialCredit,
      });

      transaction.save((err) => {
        if (err) { return; }

        channel.ack(msg);
      });
    }
  });
}

export default transactionsQueue;
