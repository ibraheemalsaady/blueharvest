import path from 'path';
import fs from 'fs';
import Yaml from 'js-yaml';
import Config from 'config';

const file = path.resolve('config', 'swagger.yaml');
const doc = Yaml.safeLoad(fs.readFileSync(file, 'utf8'));

/**
 * Returns the swagger API docs as json
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
function swaggerCtrl(req, res) {
  doc.host = req.headers.host;

  // Only allow HTTPS in production
  if (Config.production) {
    doc.schemes = ['https'];
  }

  return res.send(doc);
}

export default swaggerCtrl;
