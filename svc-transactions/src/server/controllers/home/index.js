import swaggerCtrl from './swagger';
import healthCheckCtrl from './healthcheck';

export default { swaggerCtrl, healthCheckCtrl };
