import { Transaction } from 'models';

/**
 * List transactions for customer
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function listCtrl(req, res) {
  try {
    const { id } = req.params;

    const transactions = await Transaction.find({ customerId: id });

    return res.send({ transactions });
  } catch (err) {
    return res.status(400).json({ message: 'something went wrong' });
  }
}

export default listCtrl;
