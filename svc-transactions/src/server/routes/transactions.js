import Express from 'express';
import Transactions from 'controllers/transactions';

const Router = Express.Router();

Router.route('/:id')
  .get(
    Transactions.listCtrl,
  );

export default Router;
