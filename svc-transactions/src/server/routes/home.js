import Express from 'express';
import Home from 'controllers/home';

const Router = Express.Router();

Router.get('/', Home.healthCheckCtrl);

Router.get('/swagger', Home.swaggerCtrl);

export default Router;
