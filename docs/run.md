# Running the Application
The application/services uses docker and docker-compose to package the application and run the instances needed.

There is a network between the services called `blueharvest`, you should create this network name before running the services.

To create the network, just type `docker network create blueharvest` in the console.

There are 4 services, RabbitMQ, accounts, transactions, the frontend. 
Have a terminal window open for each service (4 terminal windows).

**Make sure you have Docker and docker-compose commands installed**

**RabbitMQ service has to run first in order for the accounts service and transactions to work**.

**On Terminal #1 (RabbitMQ)**
- Navigate to the `svc-accounts` directory
- open the `.env` file and change the `CLOUDAMQP_URL` to amqp://rabbitmq
- Run `docker-compose up rabbitmq` to run RabbitMQ instance
- Service url: `http://localhost:15672` (Credentials are guest & guest for username & password)

**On Terminal #2 (Accounts)**
- Navigate to the `svc-accounts` directory
- Open the `.env` file and change MONGO_URL to point to `mongodb://mongo/blueharvest`
- Run `docker-compose up api-accounts` to run the accounts service
- Service url: `http://localhost:5000`

**On Terminal #3 (Transactions)**
- Navigate to the `svc-transactions` directory
- Open the `.env` file and change MONGO_URL to point to `mongodb://mongotrans/blueharvest`
- Run `docker-compose up api-transactions` to run the transactions service
- Service url: `http://localhost:5001`


**On Terminal #4 (Frontend)**
You can try to run this service with docker, however, I'll just go with a simple `npm start`

- Navigate to the `frontend` directory
- Open the `.env` file and change API_ACCOUNT_URL to point to `http://localhost:5000` and API_TRANSACTIONS_URL to point to `http://localhost:5001
- Run `npm start`
- Service url: `http://localhost:3000`

All services will run under one network, therefor, they will all be accessable via the service name we defined in the compose file. (docker-compose magic!)

## Environment Variables 
A `.env` file is already provided. But just in case, here are the environment variables.

**Frontend**
```
NODE_ENV=development
CI=true
PORT=3000
VERBOSE=true
DEBUG=express:*
REDUX_LOGGER=false
ACCOUNTS_API_URL=http://localhost:5000
TRANSACTIONS_API_URL=http://localhost:5001
```

**Accounts**
```
PORT=5000
MONGO_URL=mongodb://mongo/blueharvest
CLOUDAMQP_URL=amqp://rabbitmq
```

**Transactions**
```
PORT=5001
MONGO_URL=mongodb://mongotrans/blueharvest
CLOUDAMQP_URL=amqp://rabbitmq
```

### Testing
For running the end-to-end/integration test, you need to run mongo first. Have two terminal windows open. Also no need to run RabbitMQ since its will not publish messages on test environment.

Here are steps to run the tests:

#### Testing svc-accounts
**On Terminal #1 (mongo)**
- Navigate to the `svc-accounts` directory 
- Run compose by typing `docker-compose up mongo` (this will run the mongo instance)

**On Terminal #2 (test)**
- Navigate to the `svc-accounts` directory 
- In the `.env` file, change the MONGO_URL to point to localhost (mongodb://localhost:1000/blueharvest)
- run the tests by typing `npm test`

#### Testing svc-transactions
**On Terminal #1 (mongo)**
- Navigate to the `svc-transactions` directory 
- Run compose by typing `docker-compose up mongotrans` (this will run the mongo instance)

**On Terminal #2 (test)**
- Navigate to the `svc-transactions` directory 
- In the `.env` file, change the MONGO_URL to point to localhost (mongodb://localhost:1001/blueharvest)
- run the tests using npm by typing `npm test`


## Known Problems
You might run into problems of RabbitMQ not connecting, or mongo not connecting. This is probably because you already ran docker-compose up [service]. What you need to do on the service directory of whatever the service is, just do `docker-compose down`, then do `docker-compose up [service]`.