# The Architecture
I followed a Microserive architecture where each service has its own database.

![Sys Diagram](./blue-harvest-sys-diagram.png)

### Accounts Service
This services is responsible for the following.

- Create a customer (name, surname, balance)
- Create an account for an existing customer 
- List all customers
- List all accounts for a customer
- Details of a customer

### Transactions Service
This service is responsible for transactions of a certain account

- List all transactions for a customer account

### RabbitMQ
Accounts service will publish messages to a RabbitMQ queue when an account is created. A subscriber worker in the transactions service is responsibile to read from the queue and will process any message published to that queue.

### Things that were not taken into account for time constraints
- Logging & Monitoring
- Request param validations
- Swagger API documentation
- CI/CD Pipleline setup
- Securing API's and published messages
- Swagger API Docs
- Better error handling

