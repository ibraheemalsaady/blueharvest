/*
    Rosie is a factory for building JavaScript objects, it will basically mock our models.
    Rosie docs: https://github.com/rosiejs/rosie

    Faker is used for creating random/fake data
    Faker docs: https://github.com/marak/Faker.js/
*/
import { Factory } from 'rosie';
import Faker from 'faker';

const models = {};

models.Customer = Factory.define('User')
  .attrs({
    name: () => Faker.name.firstName(),
    surname: () => Faker.name.lastName(),
    balance: () => Faker.random.number(100, 9999),
  });


export default models;
