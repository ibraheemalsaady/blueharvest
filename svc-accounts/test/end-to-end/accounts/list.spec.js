import { expect } from 'chai';
import Helper from 'helper';
import Request from 'supertest';
import app from 'app';
import { createObjectId } from 'models/schemas';

describe('GET: /accounts/customer/:id', () => {
  let customer = null;

  before(async () => {
    customer = await Helper.createCustomer();
    await Helper.createAccount(customer);
  });

  it('should create an account for a customer', async () => {
    const res = await Request(app)
      .get(`/accounts/customer/${customer._id}`)
      .expect(200);

    expect(res.status).to.equal(200);
    expect(res.body).to.have.property('accounts');
  });

  it('should create an account for a customer', async () => {
    const res = await Request(app)
      .get(`/accounts/customer/${createObjectId()}`)
      .expect(404);

    expect(res.status).to.equal(404);
    expect(res.body).to.have.property('message');
  });

  /*
    - @TODO: More cases to be added when validations are added on the endpoints
  */
});
