import { expect } from 'chai';
import Helper from 'helper';
import Request from 'supertest';
import app from 'app';

describe('POST: /accounts', () => {
  let customer = null;

  before(async () => {
    customer = await Helper.createCustomer();
  });

  it('should create an account for a customer', async () => {
    const payload = {
      customerId: customer._id,
      initialCredit: Helper.Faker.random.number(100, 9999),
    };

    const res = await Request(app)
      .post('/accounts')
      .send(payload)
      .expect(200);

    expect(res.status).to.equal(200);
    expect(res.body).to.have.property('_id');
  });

  /*
    - @TODO: More cases to be added when validations are added on the endpoints
  */
});
