import { expect } from 'chai';
import Helper from 'helper';
import Request from 'supertest';
import app from 'app';

describe('GET: /customers', () => {
  before(async () => {
    await Helper.createCustomer();
  });

  it('list all customers', async () => {
    const res = await Request(app)
      .get('/customers')
      .expect(200);

    expect(res.status).to.equal(200);
    expect(res.body).to.have.property('customers');
    expect(res.body.customers).to.be.an('array');
  });

  /*
    - @TODO: More cases to be added when validations are added on the endpoints
  */
});
