import { expect } from 'chai';
import Helper from 'helper';
import Request from 'supertest';
import app from 'app';
import { createObjectId } from 'models/schemas';

describe('GET: /customers/:id', () => {
  let customer = null;

  before(async () => {
    customer = await Helper.createCustomer();
  });

  it('get the customer based on id', async () => {
    const res = await Request(app)
      .get(`/customers/${customer._id}`)
      .expect(200);

    expect(res.status).to.equal(200);
    expect(res.body).to.have.property('_id');
  });

  it('return 404 if provided invalid id', async () => {
    const res = await Request(app)
      .get(`/customers/${createObjectId()}`)
      .expect(404);

    expect(res.status).to.equal(404);
    expect(res.body).to.have.property('message');
  });

  it('return 400 if provided empty id', async () => {
    const res = await Request(app)
      .get(`/customers/${null}`)
      .expect(400);

    expect(res.status).to.equal(400);
    expect(res.body).to.have.property('message');
  });

  it('return 400 if provided not mongoose id pattern', async () => {
    const res = await Request(app)
      .get('/customers/wjfaj2and')
      .expect(400);

    expect(res.status).to.equal(400);
    expect(res.body).to.have.property('message');
  });

  /*
    - @TODO: More cases to be added when validations are added on the endpoints
  */
});
