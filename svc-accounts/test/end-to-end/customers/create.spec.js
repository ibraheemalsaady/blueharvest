import { expect } from 'chai';
import Factory from 'factory';
import Request from 'supertest';
import app from 'app';

describe('POST: /customers', () => {
  let customer = null;

  before(async () => {
    customer = Factory.Customer.build();
  });

  it('should create a customer', async () => {
    const res = await Request(app)
      .post('/customers')
      .send(customer)
      .expect(200);

    expect(res.status).to.equal(200);
    expect(res.body).to.have.property('_id');
  });

  /*
    - @TODO: More cases to be added when validations are added on the endpoints
  */
});
