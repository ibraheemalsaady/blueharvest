import Faker from 'faker';
import Factory from 'factory';
import { Customer, Account } from 'models/schemas';

/**
 * Creates a mock customer
 * @param {*} overrides overrides the default values of the customer model if provided
 */
async function createCustomer(overrides = {}) {
  const data = Factory.Customer.build(overrides);

  const customer = new Customer(data);

  await customer.save();

  return customer;
}

/**
 * Add account to an existing customer
 * @param {*} customer customer object
 * @param {*} initialCredit
 */
async function createAccount(customer, initialCredit = Faker.random.number(100, 9999)) {
  const data = {
    _customer: customer,
    initialCredit,
  };

  const account = new Account(data);

  await account.save();

  return account;
}

export default { Faker, createCustomer, createAccount };
