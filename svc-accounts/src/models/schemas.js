import mongoose, { Schema } from 'mongoose';

const { ObjectId } = Schema;

function createObjectId() {
  return mongoose.Types.ObjectId();
}

const CustomerSchema = new Schema({
  id: ObjectId,
  name: { type: String },
  surname: { type: String },
  balance: { type: Number },
  createdAt: { type: Date, default: Date.now },
});

const AccountSchema = new Schema({
  id: ObjectId,
  _customer: { type: Schema.Types.ObjectId, ref: 'Customer' },
  initialCredit: { type: Number },
  createdAt: { type: Date, default: Date.now },
});

const Customer = mongoose.model('Customer', CustomerSchema);
const Account = mongoose.model('Account', AccountSchema);

export default {
  Customer,
  Account,
};

export { createObjectId, Customer, Account };
