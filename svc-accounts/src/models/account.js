import Boom from 'boom';
import { Account, Customer } from 'models/schemas';
import RabbitMQ from 'utils/rabbitmq';

async function add(payload) {
  try {
    const account = new Account(payload);

    await account.save();

    if (payload.initialCredit > 0) {
      RabbitMQ.publish('transactions', JSON.stringify({
        customerId: payload.customer,
        accountId: account._id,
        initialCredit: payload.initialCredit,
      }));
    }

    return account;
  } catch (err) { return Promise.reject(Boom.badRequest(err)); }
}

async function list(id) {
  try {
    const customer = await Customer.findOne({ _id: id });

    if (!customer) { return Promise.reject(Boom.notFound('Invalid customer')); }

    const accounts = await Account.find({ _customer: customer });

    return accounts;
  } catch (err) { return Promise.reject(Boom.badRequest(err)); }
}

export default {
  add, list,
};
