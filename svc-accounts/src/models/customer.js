import Boom from 'boom';
import { Customer } from 'models/schemas';

async function add(payload) {
  try {
    const customer = new Customer(payload);

    await customer.save();

    return customer;
  } catch (err) { return Promise.reject(Boom.badRequest(err)); }
}

async function find(id) {
  try {
    if (!id) { return Promise.reject(Boom.badRequest('invalid id provided')); }

    const customer = await Customer.findOne({ _id: id });

    if (!customer) { return Promise.reject(Boom.notFound('customer does not exist')); }

    return customer;
  } catch (err) { return Promise.reject(Boom.badRequest(err)); }
}

async function list() {
  try {
    const customers = await Customer.find({});

    return customers;
  } catch (err) { return Promise.reject(Boom.badRequest(err)); }
}

export default {
  add, find, list,
};
