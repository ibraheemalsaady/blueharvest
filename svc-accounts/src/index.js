import dotEnv from 'dotenv';
import http from 'http';
import debug from 'debug';
import Express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import Routes from 'routes';
import ConnectMongo from 'utils/mongo';
import RabbitMQ from 'utils/rabbitmq';
import Config from 'config';

const serverDebugger = debug('account:server');
// initialize express
const app = Express();

// connecting mongo db
ConnectMongo();

RabbitMQ.start();

// config dotenv
dotEnv.config();

// using body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(cors());

// initialize routes
app.use('/', Routes.Home);
app.use('/customers', Routes.Customers);
app.use('/accounts', Routes.Accounts);

// global error handling
app.use((err, req, res, next) => {
  let error = {};
  let code;

  // swaggerize validation error
  if (err.details) {
    error.message = err.details[0].message;
    error.details = err.details;
    code = err.status;
  } else if (err.isBoom) {
    error = err.output.payload;
    code = err.output.statusCode;
  }

  if (!Config.production) { error.stack = err.toString(); }

  return res.status(code).json(error);
});

const port = process.env.PORT || 5000;
const server = http.createServer(app);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string'
    ? `Pipe ${port}`
    : `Port ${port}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
  case 'EACCES':
    console.error(`${bind} requires elevated privileges`);
    process.exit(1);
    break;
  case 'EADDRINUSE':
    console.error(`${bind} is already in use`);
    process.exit(1);
    break;
  default:
    throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string'
    ? `pipe ${addr}`
    : `port ${addr.port}`;
  serverDebugger(`Listening on ${bind}`);
}

server.listen(port, () => console.log(`server listening on port ${port}`));
server.on('error', onError);
server.on('listening', onListening);

// Catch PM2 termination signal
process.on('SIGINT', () => {
  process.exit(0);
});

export default app;
