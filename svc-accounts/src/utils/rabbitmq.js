import amqp from 'amqplib';
import config from 'config';

let amqpConn = null;
let pubChannel = null;
// const offlinePubQueue = [];

function closeOnErr(err) {
  if (!err) { return false; }

  console.error('[AMQP] error', err);
  amqpConn.close();
  return true;
}

/**
 * Start publisher channel
 */
async function startPublisher() {
  try {
    const channel = await amqpConn.createChannel();

    channel.on('error', (err) => {
      console.error('[AMQP] channel error', err.message);
    });

    channel.on('close', () => {
      console.log('[AMQP] channel closed');
    });

    pubChannel = channel;
  } catch (err) {
    if (closeOnErr(err)) { return; }

    throw new Error(err);
  }
}

/**
 * Publish message to queue
 * @param {*} queue
 * @param {*} content
 */
async function publish(queue, content) {
  // don't publish messages if in testing mode
  if (config.testing) { return; }

  try {
    await pubChannel.assertQueue(queue);
    pubChannel.sendToQueue(queue, Buffer.from(content));
  } catch (err) {
    throw new Error(err);
  }
}

async function whenConnected() {
  await startPublisher();
}

/**
 * Starts RabbitMQ Connection
 */
async function start() {
  try {
    // don't publish messages if in testing mode
    if (config.testing) { return; }

    amqpConn = await amqp.connect(`${config.rabbitmqUrl}?heartbeat=60`);

    amqpConn.on('error', (err) => {
      console.log('rabbit mq error here...');
      throw new Error(err);
    });
    amqpConn.on('close', () => {
      console.error('[AMQP] reconnecting');
      return setTimeout(start, 1000);
    });

    console.log('rabbitmq connected...');

    whenConnected();

    // return amqpConn;
  } catch (err) {
    throw new Error(err);
  }
}

export default {
  start,
  publish,
};
