import Customer from 'models/customer';

/**
 * List customers controller
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function listCtrl(req, res, next) {
  try {
    const customers = await Customer.list();

    return res.send({ customers });
  } catch (err) {
    return next(err);
  }
}


export default listCtrl;
