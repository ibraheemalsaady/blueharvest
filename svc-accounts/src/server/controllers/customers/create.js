// import { Customer } from 'models';
import Customer from 'models/customer';

/**
 * Creates customer controller
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function createCtrl(req, res, next) {
  try {
    const payload = {
      name: req.body.name,
      surname: req.body.surname,
      balance: req.body.balance,
    };

    const customer = await Customer.add(payload);

    return res.send(customer);
  } catch (err) {
    return next(err);
  }
}


export default createCtrl;
