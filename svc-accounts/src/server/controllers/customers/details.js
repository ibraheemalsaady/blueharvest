import Customer from 'models/customer';

/**
 * Details customer controller
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function detailsCtrl(req, res, next) {
  try {
    const { id } = req.params;

    const customer = await Customer.find(id);

    return res.send(customer);
  } catch (err) {
    return next(err);
  }
}

export default detailsCtrl;
