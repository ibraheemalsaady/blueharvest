import createCtrl from './create';
import detailsCtrl from './details';
import listCtrl from './list';

export default { createCtrl, detailsCtrl, listCtrl };
