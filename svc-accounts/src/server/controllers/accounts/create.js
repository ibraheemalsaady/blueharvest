import Account from 'models/account';

/**
 * Creates account controller
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function createCtrl(req, res, next) {
  try {
    const payload = {
      _customer: req.body.customerId,
      initialCredit: req.body.initialCredit,
    };

    const account = await Account.add(payload);

    return res.send(account);
  } catch (err) {
    return next(err);
  }
}


export default createCtrl;
