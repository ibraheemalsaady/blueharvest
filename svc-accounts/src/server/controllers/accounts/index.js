import createCtrl from './create';
import listCtrl from './list';

export default { createCtrl, listCtrl };
