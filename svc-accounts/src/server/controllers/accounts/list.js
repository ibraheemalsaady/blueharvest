import Account from 'models/account';

/**
 * List accounts controller
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
async function listCtrl(req, res, next) {
  try {
    const { id } = req.params;

    const accounts = await Account.list(id);

    return res.send({ accounts });
  } catch (err) {
    return next(err);
  }
}

export default listCtrl;
