const uptimeDate = new Date();

/**
 * Returns health check information
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
function healthCheckCtrl(req, res) {
  res.send({
    name: 'Account Service',
    started: uptimeDate,
    uptime: `${(new Date().getTime() - uptimeDate.getTime()) / 1000} seconds`,
  });
}


export default healthCheckCtrl;
