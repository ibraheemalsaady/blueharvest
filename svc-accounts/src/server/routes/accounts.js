import Express from 'express';
import Accounts from 'controllers/accounts';

const Router = Express.Router();

Router.route('/')
  .post(
    Accounts.createCtrl,
  );

Router.route('/customer/:id')
  .get(
    Accounts.listCtrl,
  );

export default Router;
