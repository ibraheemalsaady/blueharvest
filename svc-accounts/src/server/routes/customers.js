import Express from 'express';
import Customers from 'controllers/customers';

const Router = Express.Router();

Router.route('/')
  .post(
    Customers.createCtrl,
  );
Router.route('/')
  .get(
    Customers.listCtrl,
  );

Router.route('/:id')
  .get(
    Customers.detailsCtrl,
  );

export default Router;
