import Home from './home';
import Customers from './customers';
import Accounts from './accounts';

export default {
  Home, Customers, Accounts,
};
