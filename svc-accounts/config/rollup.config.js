import path from 'path';
import json from 'rollup-plugin-json';
import babel from 'rollup-plugin-babel';
import replace from 'rollup-plugin-replace';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import eslint from 'rollup-plugin-eslint';
import { uglify } from 'rollup-plugin-uglify';

export default {
  input: 'src/index.js',
  output: {
    file: 'dist/bundle.js',
    format: 'cjs',
  },
  plugins: [
    replace({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
    resolve({
      module: true,
      modulesOnly: true,
      extensions: ['.js', '.json'],
    }),
    commonjs({
      include: 'node_modules/**',
    }),
    eslint({
      fix: true,
    }),
    babel({
      extends: path.resolve('config', '.babelrc.prod'),
      exclude: 'node_modules/**',
      runtimeHelpers: true,
    }),
    json({
      include: 'node_modules/**',
    }),
    (process.env.NODE_ENV === 'production' && uglify()),
  ],
};
