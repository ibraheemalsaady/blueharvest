require('dotenv').config();
require('./babel-register');

const path = require('path');

try {
  require(path.join('..', '/src'));
} catch (err) {
  console.error(err);
}
