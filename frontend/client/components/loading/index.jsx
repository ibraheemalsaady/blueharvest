import React, { Component } from 'react';
import { Icon } from 'antd';

import './style.scss';

class Loading extends Component {
  render() {
    return (
      <div className="loading-container">
        <Icon type='loading' style={{ fontSize: 32 }} />
      </div>
    )
  }
}

export default Loading;