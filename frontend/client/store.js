import { createStore, applyMiddleware } from 'redux'
import reducers from './reducers'
import { createLogger } from 'redux-logger';
import createSagaMiddleware, { END } from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';

const logger = createLogger();
const sagaMiddleware = createSagaMiddleware();

export default function configureStore(initialState = {}) {
  const middlewares = [
    sagaMiddleware
  ];

  if (process.env.reduxLogger && !process.env.production) {
    middlewares.push(logger);
  }

  const enhancers = [
    applyMiddleware(...middlewares)
  ];

  const store = createStore(
    reducers,
    initialState,
    composeWithDevTools(...enhancers)
  );

  store.runSaga = sagaMiddleware.run;
  store.close = () => store.dispatch(END);
  // store.asyncReducers = {}; // Async reducer registry

  return store;
}
