import {
  LIST_TRANSACTIONS_PROGRESS,
  LIST_TRANSACTIONS_SUCCESS,
  LIST_TRANSACTIONS_ERROR
} from 'types/transactions';

const initialState = {
  inProgress: false,
  error: null,
  transactions: []
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case LIST_TRANSACTIONS_PROGRESS:
      return { ...state, inProgress: true }
    case LIST_TRANSACTIONS_ERROR:
      return { ...state, error: action.error, inProgress: false };
    case LIST_TRANSACTIONS_SUCCESS:
      return { ...state, inProgress: false, transactions: action.transactions }
    default:
      return state;
  }
};