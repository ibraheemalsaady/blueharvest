import { combineReducers } from 'redux';

import createCustomer from './customers/create';
import listCustomers from './customers/list';
import detailsCustomer from './customers/details';
import createAccount from './accounts/create';
import listAccounts from './accounts/list';
import listTransactions from './transactions/list';

export default combineReducers({
  createCustomer,
  listCustomers,
  detailsCustomer,
  createAccount,
  listAccounts,
  listTransactions
});
