import {
  LIST_ACCOUNTS_PROGRESS,
  LIST_ACCOUNTS_SUCCESS,
  LIST_ACCOUNTS_ERROR
} from 'types/accounts';

const initialState = {
  inProgress: false,
  error: null,
  accounts: [],
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case LIST_ACCOUNTS_PROGRESS:
      return { ...state, inProgress: true }
    case LIST_ACCOUNTS_ERROR:
      return { ...state, error: action.error, inProgress: false };
    case LIST_ACCOUNTS_SUCCESS:
      return { ...state, inProgress: false, accounts: action.accounts }
    default:
      return state;
  }
};