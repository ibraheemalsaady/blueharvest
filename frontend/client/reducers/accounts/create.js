import {
  CREATE_ACCOUNT_PROGRESS,
  CREATE_ACCOUNT_SUCCESS,
  CREATE_ACCOUNT_ERROR
} from 'types/accounts';

const initialState = {
  inProgress: false,
  error: null,
  message: null,
  customer: null
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_ACCOUNT_PROGRESS:
      return { ...state, inProgress: true }
    case CREATE_ACCOUNT_ERROR:
      return { ...state, error: action.error };
    case CREATE_ACCOUNT_SUCCESS:
      return { ...state, inProgress: false, account: action.account }
    default:
      return state;
  }
};
