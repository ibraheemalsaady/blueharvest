import {
  DETAILS_CUSTOMER_PROGRESS,
  DETAILS_CUSTOMER_SUCCESS,
  DETAILS_CUSTOMER_ERROR
} from 'types/customers';

const initialState = {
  inProgress: false,
  error: null,
  message: null,
  customer: {
    _id: '',
    name: '',
    surname: '',
    balance: '',
    createdAt: ''
  }
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case DETAILS_CUSTOMER_PROGRESS:
      return { ...state, inProgress: true }
    case DETAILS_CUSTOMER_ERROR:
      return { ...state, error: action.error };
    case DETAILS_CUSTOMER_SUCCESS:
      return { ...state, inProgress: false, customer: action.customer }
    default:
      return state;
  }
};
