import {
  CREATE_CUSTOMER_PROGRESS,
  CREATE_CUSTOMER_SUCCESS,
  CREATE_CUSTOMER_ERROR
} from 'types/customers';

const initialState = {
  inProgress: false,
  error: null,
  message: null,
  customer: null
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case CREATE_CUSTOMER_PROGRESS:
      return { ...state, inProgress: true }
    case CREATE_CUSTOMER_ERROR:
      return { ...state, error: action.error };
    case CREATE_CUSTOMER_SUCCESS:
      return { ...state, inProgress: false, customer: action.customer }
    default:
      return state;
  }
};
