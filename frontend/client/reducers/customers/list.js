import {
  LIST_CUSTOMERS_PROGRESS,
  LIST_CUSTOMERS_SUCCESS,
  LIST_CUSTOMERS_ERROR
} from 'types/customers';

const initialState = {
  inProgress: false,
  error: null,
  customers: [],
  data: [{
    key: -1,
    name: ''
  }],
  columns: [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Surname',
      dataIndex: 'surname',
      key: 'surname',
    },
    {
      title: 'Balance',
      dataIndex: 'balance',
      key: 'balance',
    },
    {
      title: 'Action',
      key: 'action'
    }
  ]
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case LIST_CUSTOMERS_PROGRESS:
      return { ...state, inProgress: true }
    case LIST_CUSTOMERS_ERROR:
      return { ...state, error: action.error, inProgress: false };
    case LIST_CUSTOMERS_SUCCESS:
      const tableData = action.customers.map((item) => ({
        key: item._id,
        name: item.name,
        surname: item.surname,
        balance: item.balance
      }));

      return { ...state, inProgress: false, data: tableData, customers: action.customers }
    default:
      return state;
  }
};