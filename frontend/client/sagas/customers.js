import { takeLatest, put, call } from 'redux-saga/effects';
import {
  CREATE_CUSTOMER_PROGRESS,
  CREATE_CUSTOMER_SUCCESS,
  CREATE_CUSTOMER_ERROR,
  LIST_CUSTOMERS_PROGRESS,
  LIST_CUSTOMERS_SUCCESS,
  LIST_CUSTOMERS_ERROR,
  DETAILS_CUSTOMER_PROGRESS,
  DETAILS_CUSTOMER_SUCCESS,
  DETAILS_CUSTOMER_ERROR
} from 'types/customers';
import MessageManager from 'utils/message-manager';
import Fetch from 'utils/fetch';
import Endpoints from 'utils/endpoints';


function* create(action) {
  try {
    const res = yield call(Fetch.post, `${Endpoints.services.accounts}/customers`, action.payload);

    yield put({ type: CREATE_CUSTOMER_SUCCESS, customers: res });
    yield put({ type: LIST_CUSTOMERS_PROGRESS });

    MessageManager.success('Customer has been created');
  } catch (err) {
    MessageManager.error('something went wrong');

    yield put({ type: CREATE_CUSTOMER_ERROR, error: 'something went wrong' });
  }
}

function* list() {
  try {
    const res = yield call(Fetch.get, `${Endpoints.services.accounts}/customers`);

    yield put({ type: LIST_CUSTOMERS_SUCCESS, customers: res.customers });
  } catch (err) {
    MessageManager.error('something went wrong');

    yield put({ type: LIST_CUSTOMERS_ERROR, error: 'something went wrong' });
  }
}

function* details(action) {
  try {
    const res = yield call(Fetch.get, `${Endpoints.services.accounts}/customers/${action.id}`);

    yield put({ type: DETAILS_CUSTOMER_SUCCESS, customer: res });
  } catch (err) {
    MessageManager.error('something went wrong');

    yield put({ type: DETAILS_CUSTOMER_ERROR, error: 'something went wrong' });
  }
}

export default function* watcher() {
  yield takeLatest(CREATE_CUSTOMER_PROGRESS, create);
  yield takeLatest(LIST_CUSTOMERS_PROGRESS, list);
  yield takeLatest(DETAILS_CUSTOMER_PROGRESS, details);
};