import customersSaga from './customers';
import accountsSaga from './accounts';

export default function* Sagas() {
  yield [
    customersSaga(),
    accountsSaga()
  ]
};