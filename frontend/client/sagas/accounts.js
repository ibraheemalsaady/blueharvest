import { takeLatest, put, call } from 'redux-saga/effects';
import {
  CREATE_ACCOUNT_PROGRESS,
  CREATE_ACCOUNT_SUCCESS,
  CREATE_ACCOUNT_ERROR,
  LIST_ACCOUNTS_PROGRESS,
  LIST_ACCOUNTS_SUCCESS,
  LIST_ACCOUNTS_ERROR
} from 'types/accounts';
import { LIST_TRANSACTIONS_SUCCESS } from 'types/transactions';
import MessageManager from 'utils/message-manager';
import Fetch from 'utils/fetch';
import Endpoints from 'utils/endpoints';


function* create(action) {
  try {
    const res = yield call(Fetch.post, `${Endpoints.services.accounts}/accounts`, action.payload);

    yield put({ type: CREATE_ACCOUNT_SUCCESS, account: res });
    yield put({ type: LIST_ACCOUNTS_PROGRESS, id: action.payload.customerId });

    MessageManager.success('Account has been created');
  } catch (err) {
    MessageManager.error('something went wrong');

    yield put({ type: CREATE_ACCOUNT_ERROR, error: 'something went wrong' });
  }
}

function* list(action) {
  try {
    const res = yield call(Fetch.get, `${Endpoints.services.accounts}/accounts/customer/${action.id}`);
    const transactions = yield call(Fetch.get, `${Endpoints.services.transactions}/transactions/${action.id}`);

    yield put({ type: LIST_ACCOUNTS_SUCCESS, accounts: res.accounts });
    yield put({ type: LIST_TRANSACTIONS_SUCCESS, transactions: transactions.transactions });
  } catch (err) {
    MessageManager.error('something went wrong');

    yield put({ type: LIST_ACCOUNTS_ERROR, error: 'something went wrong' });
  }
}

export default function* watcher() {
  yield takeLatest(CREATE_ACCOUNT_PROGRESS, create);
  yield takeLatest(LIST_ACCOUNTS_PROGRESS, list);
};