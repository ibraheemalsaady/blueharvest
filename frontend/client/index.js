import React from 'react';
import ReactDOM from 'react-dom';

import configureStore from './store';
import rootSaga from './sagas';
import registerServiceWorker from './registerServiceWorker';
import Root from 'containers/root';

import './index.scss';

const initialState = {};
const store = configureStore(initialState);
store.runSaga(rootSaga);

function renderRoot() {
  ReactDOM.render(
    <Root store={store} />
    , document.getElementById('root'));
}

renderRoot();
registerServiceWorker();

// fetch(`/i18n/${locale}.json`)
//   .then((res) => res.json())
//   .then((json) => {
//     addLocaleData([
//       ...reactIntlLocaleEn
//     ]);

//     initSwagger().then(() => {
      
//     });
//   })
//   .catch((err) => {
//     console.error(err);
//   });

