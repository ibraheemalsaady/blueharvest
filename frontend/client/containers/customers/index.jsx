import React, { Component } from 'react';
import { Button, Table } from 'antd';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { customersActions } from 'actions';
import CustomerForm from 'containers/customers/form';
import { Link } from 'react-router-dom';
import { cloneDeep } from 'lodash';

class Customers extends Component {
  state = {
    isFormVisible: false
  }

  showForm() {
    this.setState({ isFormVisible: !this.state.isFormVisible });
  }

  hideForm() {
    this.setState({ isFormVisible: false });
  }

  onSubmit(values) {
    this.props.customersActions.create(values);
    this.setState({ isFormVisible: false });
  }

  getTableColumns() {
    const clonedColumns = cloneDeep(this.props.columns);

    if (!clonedColumns[clonedColumns.length - 1].render) {
      clonedColumns[clonedColumns.length - 1].render = (text, record) => {
        return <Link to={`/dashboard/customers/${record.key}`}><span><b>View</b></span></Link>
      }
    }

    return clonedColumns;
  }

  componentWillMount() {
    this.props.customersActions.list();
  }

  render() {
    return (
      <div className="customers-list">
        <h1>Customers</h1>
        <Button type="primary" onClick={this.showForm.bind(this)}>Create New Customer</Button>

        <CustomerForm
          visible={this.state.isFormVisible}
          onSubmit={this.onSubmit.bind(this)}
          onClose={this.hideForm.bind(this)}
        />

        <Table
          loading={this.props.inProgress}
          columns={this.getTableColumns()}
          dataSource={this.props.data} />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  inProgress: state.listCustomers.inProgress,
  data: state.listCustomers.data,
  customers: state.listCustomers.customers,
  columns: state.listCustomers.columns,
});

const mapDispatchToProps = dispatch => ({
  customersActions: bindActionCreators(customersActions, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Customers);