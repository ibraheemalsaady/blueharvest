import React, { Component } from 'react';
import { Form, Input, InputNumber, Row, Col, Modal } from 'antd';

const FormItem = Form.Item;

class CustomerForm extends Component {
  handleSubmit = (e) => {
    if (e) { e.preventDefault(); }

    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.onSubmit(values);
      }
    });
  }

  onClose() {
    this.props.onClose();
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const formItemLayoutWithOutLabel = {
      wrapperCol: {
        xs: { span: 24, offset: 0 },
        sm: { span: 24, offset: 0 },
        md: { span: 24, offset: 0 },
        lg: { span: 24, offset: 0 },
      },
    };

    return (
      <Modal
        className=''
        visible={this.props.visible}
        title="New Customer"
        okText="Create"
        cancelText="Cancel"
        onOk={this.handleSubmit.bind(this)}
        onCancel={this.onClose.bind(this)}
        width="60%">
        <Form onSubmit={this.handleSubmit} className='form-padding'>
          <Row gutter={20}>
            <Col span={12}>
              <FormItem {...formItemLayoutWithOutLabel} className='fill-x'>
                {getFieldDecorator('name', {
                  rules: [{ required: true, message: 'This field is required' }],
                })(
                  <Input type="text" placeholder="Name" />
                )}
              </FormItem>
            </Col>
            <Col span={12}>
              <FormItem {...formItemLayoutWithOutLabel} className='fill-x'>
                {getFieldDecorator('surname', {
                  rules: [{ required: true, message: 'This field is required' }],
                })(
                  <Input type="text" placeholder="Surname" />
                )}
              </FormItem>
            </Col>
          </Row>
          <Row gutter={20}>
            <Col span={24}>
              <FormItem {...formItemLayoutWithOutLabel} className='fill-x'>
                {getFieldDecorator('balance', {
                  rules: [{ required: true, message: 'This field is required' }],
                })(
                  <InputNumber type="text" placeholder="Balance" className='fill-x' />
                )}
              </FormItem>
            </Col>
          </Row>
        </Form>
      </Modal>
    )
  }
}


const WrappedCustomerForm = Form.create({})(CustomerForm);

export default WrappedCustomerForm;