import React, { Component } from 'react';
import { connect } from 'react-redux';
import { withRouter } from 'react-router'
import { Switch, Route, Link } from 'react-router-dom';
import { Layout, Menu, Icon } from 'antd';
import Customers from 'containers/customers';
import Accounts from 'containers/accounts';

import './App.scss';

const { Header, Sider, Content } = Layout;

class App extends Component {
  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  render() {
    return (
      <Layout className="layout">
        <Sider
          className="sider"
          trigger={null}
          collapsible
          collapsed={this.state.collapsed}>
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
            {/* <Menu.Item key="1">
              <Link to={'/dashboard'}>
                <Icon type="home" />
                <span>Dashboard</span>
              </Link>
            </Menu.Item> */}
            <Menu.Item key="1">
              <Link to={'/dashboard/customers'}>
                <Icon type="user" />
                <span>Customers</span>
              </Link>
            </Menu.Item>
            {/* <Menu.Item key="2">
              <Link to={'/dashboard/customers'}>
                <Icon type="upload" />
                <span>Transactions</span>
              </Link>
            </Menu.Item> */}
          </Menu>
        </Sider>
        <Layout>
          <Header className="header" style={{ background: '#fff', padding: 0 }}>
            <Icon
              className="trigger"
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
          </Header>
          <Content style={{ margin: '24px 16px', padding: 24, background: '#fff', minHeight: 280 }}>
            <Switch>
              <Route exact path="/dashboard/customers" component={Customers} />
              <Route exact path="/dashboard/customers/:id" component={Accounts} />
              {/* <Route exact path="/dashboard/transactions" component={Statuses} /> */}
            </Switch>
          </Content>
        </Layout>
      </Layout>
    );
  }
}

const mapStateToProps = state => ({
  // user: state.user
});

const mapDispatchToProps = dispatch => ({
  // agendaActions: bindActionCreators(agendaActions, dispatch)
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps
)(App));
