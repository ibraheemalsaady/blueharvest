import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import { Button } from 'antd';
import { customersActions, accountsActions } from 'actions';
import AccountForm from 'containers/accounts/form';

import './style.scss'
class CustomerDetails extends Component {
  state = {
    isFormVisible: false
  }

  showForm() {
    this.setState({ isFormVisible: !this.state.isFormVisible });
  }

  hideForm() {
    this.setState({ isFormVisible: false });
  }

  onSubmit(values) {
    this.props.accountsActions.create({
      customerId: this.props.match.params.id,
      ...values
    });

    this.setState({ isFormVisible: false });
  }

  getTransactions(id) {
    return this.props.transactions.filter((item) => item.accountId === id);
  }

  componentWillMount() {
    this.props.customersActions.details(this.props.match.params.id);
    this.props.accountsActions.list(this.props.match.params.id);
  }

  render() {
    return (
      <div className="customer-details">
        <h1>{this.props.customer.name} {this.props.customer.surname}</h1>

        <Button type="primary" onClick={this.showForm.bind(this)}>Create New Account</Button>

        <AccountForm
          visible={this.state.isFormVisible}
          onSubmit={this.onSubmit.bind(this)}
          onClose={this.hideForm.bind(this)}
        />

        <div className="accounts-list">
          {
            this.props.accounts.map((item, index) => {
              return (
                <div className="account-item" key={item._id}>
                  <div className="name">Account #{index + 1}</div>
                  <div>
                    {
                      this.getTransactions(item._id).map((item, index) => {
                        return (
                          <div className="transaction-item" key={item._id}>
                            <div className="name">Transaction (#{index + 1})</div>
                            <div className="credit"><strong>Credit: </strong>{item.credit}</div>
                          </div>
                        )
                      })
                    }
                  </div>
                  <div className="separator"></div>
                </div>
              )
            })
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => ({
  customer: state.detailsCustomer.customer,
  inProgress: state.listAccounts.inProgress,
  accounts: state.listAccounts.accounts,
  transactions: state.listTransactions.transactions
  // data: state.listAccounts.data,
  // columns: state.listAccounts.columns,
});

const mapDispatchToProps = dispatch => ({
  customersActions: bindActionCreators(customersActions, dispatch),
  accountsActions: bindActionCreators(accountsActions, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CustomerDetails);