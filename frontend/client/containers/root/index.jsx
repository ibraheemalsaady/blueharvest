
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Route, Redirect, Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import {
  history
} from 'utils/routes';

import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';

import App from 'containers/app';

class Root extends Component {
  render() {
    return (
      <Provider store={this.props.store}>
        <LocaleProvider locale={enUS}>
          <Router history={history}>
            <Switch>
              <Route exact path="/" render={() => (<Redirect to="/dashboard/customers" />)} />
              <Route path="/dashboard/customers" component={App} />
              {/* <PropsRoute path="/login" component={Login} />
              <PropsRoute path="/set-profile/:token" component={SetProfile} /> */}
              {/* <Route component={NoMatch} /> */}
            </Switch>
          </Router>
        </LocaleProvider>
      </Provider>
    );
  }
}

const mapStateToProps = state => ({
  // user: state.user
});

const mapDispatchToProps = dispatch => ({
  // userActions: bindActionCreators(userActions, dispatch)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Root);