import { LIST_TRANSACTIONS_PROGRESS } from 'types/transactions';

function list(id) {
  return {
    type: LIST_TRANSACTIONS_PROGRESS,
    id
  }
}

export default {
  list
};
