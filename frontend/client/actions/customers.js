import { CREATE_CUSTOMER_PROGRESS, LIST_CUSTOMERS_PROGRESS, DETAILS_CUSTOMER_PROGRESS } from 'types/customers';

function create(payload) {
  return {
    type: CREATE_CUSTOMER_PROGRESS,
    payload
  }
};

function list() {
  return {
    type: LIST_CUSTOMERS_PROGRESS
  }
};

function details(id) {
  return {
    type: DETAILS_CUSTOMER_PROGRESS,
    id
  }
};

export default {
  create,
  list,
  details
};
