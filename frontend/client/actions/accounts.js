import { CREATE_ACCOUNT_PROGRESS, LIST_ACCOUNTS_PROGRESS } from 'types/accounts';

function create(payload) {
  return {
    type: CREATE_ACCOUNT_PROGRESS,
    payload
  }
};

function list(id) {
  return {
    type: LIST_ACCOUNTS_PROGRESS,
    id
  }
}

export default {
  create,
  list
};
