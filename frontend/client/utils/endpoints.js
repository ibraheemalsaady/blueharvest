const services = {
  accounts: process.env.accountsApiUrl,
  transactions: process.env.transactionsApiUrl
};

export default { services };
