import queryString from 'query-string';

function parser(search) {
  return queryString.parse(search);
};

export default parser;