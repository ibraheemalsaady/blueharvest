function handle(err) {
  if (err.response && err.response.body && err.response.body.error) {
    return err.response.body.error.message;
  }

  if (err.body && err.body.error.message) { return err.body.error.message };

  if (typeof err === 'string') { return err };

  if (err.message) { return err.message };

  return err.toString();
}

export default handle;
