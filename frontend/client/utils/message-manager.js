import { message } from 'antd';

function success(text) {
  message.success(text);
};

function error(text) {
  message.error(text);
};

function warning(text) {
  message.warning(text);
};

export default {
  success,
  error,
  warning
};
