import { notification } from 'antd';

function success(title, message) {
  notification.success({
    message: title,
    description: message,
  });
};

function error(title, message) {
  notification.error({
    message: title,
    description: message,
  });
};

export default {
  success,
  error
};
