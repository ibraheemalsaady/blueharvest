const express = require('express');
const path = require('path');

const app = express();

// Serve static files from the React app
app.use(express.static(path.resolve('build')));

app.get('*', (req, res) => {
  res.sendFile(path.resolve('build/index.html'));
});

const port = 3000;
app.listen(port);

console.log(`React server running on ${port}`);
